const https = require('https')
const redis = require("redis");
const { promisify } = require("util")

const requestPromise = ((hostname, port, path, method, headers, data) => {
  const options = {
    hostname,
    port,
    path,
    method,
    headers: {
      ...headers,
      'Content-Type': 'application/json',
      'Content-Length': data.length,
   }
  };
  return new Promise((resolve, reject) => {
    const req = https.request(options,
      (res) => {
        let body = ''
        res.on('data', (chunk) => (body += chunk.toString()))
        res.on('error', reject)
        res.on('end', () => {
          if (res.statusCode >= 200 && res.statusCode <= 299) {
            resolve({statusCode: res.statusCode, headers: res.headers, body: JSON.parse(body)})
          } else {
            resolve({statusCode: res.statusCode, headers: res.headers, body: body})
            // reject('Request failed. status: ' + res.statusCode + ', body: ' + body)
          }
        })
      });
    req.on('error', reject)
    req.write(data)
    req.end()
  })
})

const redisClient = redis.createClient();

redisClient.on("error", function(error) {
  console.error(error);
});

const redisIncrPromise = promisify(redisClient.incr).bind(redisClient)
const redisSetPromise = promisify(redisClient.set).bind(redisClient)
const redisGetPromise = promisify(redisClient.get).bind(redisClient)


module.exports = { 
  requestPromise, 
  redisIncrPromise, 
  redisSetPromise, 
  redisGetPromise, 
}
