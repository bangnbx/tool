const fs = require('fs')
const Web3 = require('web3')
const BigNumber = require('bignumber.js')
const { waitUntil } = require('async-wait-until')
const request = require('request')

const { 
  requestPromise, 
  redisIncrPromise, 
  redisSetPromise, 
  redisGetPromise, 
} = require('./utils')

const {
  wssUrl,
  web3Options,
} = require('./networkConfig')

const {
  MAX_RETRY,
  redkiteAddress,
  redkitePool,
  gas,
  gasPriceInGwei,
  untilTime,
  redkiteAbi,
  tokenAddress,
  platform,
  bnbAccountPrivateKey,
  hackedAccountPrivateKey,
  beneficiary,
  tokenAmount,
} = require(process.argv[2])

const setNonceTime = new Date(untilTime - 5 * 1000)
const sendBnbTime = new Date(untilTime - 0 * 1000)

const web3 = new Web3(new Web3.providers.WebsocketProvider(wssUrl, web3Options))

var claimSuccess = 0

const tokenAbi = JSON.parse(fs.readFileSync(`./token.json`, 'utf8'))
const tokenContract = new web3.eth.Contract(tokenAbi, tokenAddress)
var tokenDecimals

web3.eth.accounts.wallet.add(bnbAccountPrivateKey)
const bnbAccountAddress = web3.eth.accounts.wallet[web3.eth.accounts.wallet.length - 1].address
web3.eth.accounts.wallet.add(hackedAccountPrivateKey)
const hackedAddress = web3.eth.accounts.wallet[web3.eth.accounts.wallet.length - 1].address

const redkiteContract = new web3.eth.Contract(redkiteAbi, redkiteAddress)
const messageToSign = 'Red Kite User Signature'

const claim = async (address, _amount, _signature) => {
  const nonce = await redisIncrPromise(address) - 1
  redkiteContract.methods.claimTokens(
    address,
    _amount,
    _signature,
  ).send({
    from: address,
    value: 0,
    gas: gas,
    gasPrice: web3.utils.toWei(gasPriceInGwei, 'gwei'),
    nonce: nonce,
  }).on('transactionHash', (hash) => {
    console.log(`${address} claim tx: https://bscscan.com/tx/${hash}`)
  }).on('receipt', (receipt) => {
    console.log('claim receipt', receipt.status)
    return
  }).on('error', (error, receipt) => {
    console.log('error')
    console.log(error)
  })
}

const toEther = (amount, decimals) => {
  return BigNumber(Math.pow(10, decimals)).times(BigNumber(amount)).toFixed(0)
}

const doClaim = async () => {
  console.log(`Waiting until claim time ${untilTime}...`)
  await waitUntil(() => Date.now() >= untilTime, {timeout: Number.POSITIVE_INFINITY, intervalBetweenAttempts: 1})

  // claim
  const signature = await web3.eth.sign(messageToSign, hackedAddress)
  const data = {
    campaign_id: redkitePool,
    wallet_address: hackedAddress,
    signature,
  }
  const headers = {
    msgsignature: 'Red Kite User Signature',
  }

  var maxRetries = MAX_RETRY
  var res = null
  while (!res && maxRetries > 0) {
    if (platform === 'Redkite') {
      res = await requestPromise('redkite-api.polkafoundry.com', 443, '/user/claim', 'POST', headers, JSON.stringify(data))
    } else {
      res = await requestPromise('hub.gamefi.org', 443, '/api/v1/user/claim', 'POST', headers, JSON.stringify(data))
    }

    if (res.body.status !== 200) {
      res = null 
      console.log('retry when call API')
      maxRetries -= 1
    }
  }
  const _amount = res.body.data.amount
  const _signature = res.body.data.signature
  claim(hackedAddress, _amount, _signature) 

  // transfer
  const nonce = await redisIncrPromise(hackedAddress) - 1
  const tokenContract = new web3.eth.Contract(tokenAbi, tokenAddress)
  tokenContract.methods.transfer(
    beneficiary,
    toEther(tokenAmount, tokenDecimals),
  ).send({
    from: hackedAddress,
    value: 0,
    gas: gas,
    gasPrice: web3.utils.toWei(gasPriceInGwei, 'gwei'),
    nonce: nonce,
  }).on('transactionHash', (hash) => {
    console.log(`${hackedAddress} transfer tx: https://bscscan.com/tx/${hash}`)
  }).on('receipt', (receipt) => {
    console.log('transfer receipt', receipt.status)
  }).on('error', (error, receipt) => {
    console.log('error')
    console.log(error)
  })

}

const claimAndTransfer = async () => {
  // send bnb
  web3.eth.sendTransaction({
    to: hackedAddress, 
    from: bnbAccountAddress, 
    value: web3.utils.toWei('0.01', 'ether'),
    gas: gas,
    gasPrice: web3.utils.toWei(gasPriceInGwei, 'gwei'),
  }).on('transactionHash', (hash) => {
    console.log(`send bnb tx: https://bscscan.com/tx/${hash}`)
  }).on('receipt', (receipt) => {
    console.log('send bnb receipt', receipt.status)
    if (receipt.status === true) {
      doClaim()
    }
  }).on('error', (error, receipt) => {
    console.log('error')
    console.log(error)
  })

  
}

const run = async () => {
  console.log(`Waiting until set nonce time: ${setNonceTime}`)
  await waitUntil(() => Date.now() >= setNonceTime, {timeout: Number.POSITIVE_INFINITY, intervalBetweenAttempts: 1})
  tokenDecimals = await tokenContract.methods.decimals().call()
  const nonce = await web3.eth.getTransactionCount(hackedAddress)
  await redisSetPromise(hackedAddress, nonce)
  console.log('set Nonce', hackedAddress, nonce)

  console.log(`Waiting until send bnb time ${sendBnbTime}...`)
  await waitUntil(() => Date.now() >= sendBnbTime, {timeout: Number.POSITIVE_INFINITY, intervalBetweenAttempts: 1})
  claimAndTransfer()
}

run()

