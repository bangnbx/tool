const fs = require('fs')

const platform = 'Gamefi' // Redkite or Gamefi

const redkiteAddress = '0xb3d585799f2035f2BD86b3CdE680f8f9985e0f7d' 
const redkitePool = 27
const untilTime = new Date('2021-11-09T12:00:00.000Z') // open claim
const tokenAddress = '0x54523D5fB56803baC758E8B10b321748A77ae9e9' // 
const tokenAmount = '4000'

const gas = 300000 
const gasPriceInGwei = '40' 

const MAX_RETRY = 50 // when request Redkite API

const redkiteAbi = JSON.parse(fs.readFileSync(`./redkite.json`, 'utf8'))

const bnbAccountPrivateKey = ''
const hackedAccountPrivateKey = ''
const beneficiary = ''

module.exports = {
  MAX_RETRY,
  redkiteAddress,
  redkitePool,
  gas,
  gasPriceInGwei,
  untilTime,
  redkiteAbi,
  tokenAddress,
  platform,
  bnbAccountPrivateKey,
  hackedAccountPrivateKey,
  beneficiary,
  tokenAmount,
}
