const wssUrl = 'ws://3.227.10.42:8546'

const web3Options = {
  timeout: 1000,
  reconnect: {
    auto: true,
    delay: 10, 
    maxAttempts: 10000000,
    onTimeout: true,
  },
  clientConfig: {
    closeTimeout: 1000,
    keepalive: true,
    keepaliveInterval: 60,
  },
}

module.exports = { 
  wssUrl, 
  web3Options,
}

