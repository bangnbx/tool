const fs = require('fs')

const platform = 'Gamefi' // Redkite or Gamefi

const redkiteAddress = '0x4Ca59B3756BB63528Cf7424943003B15C088d4b8' // Redkite public
const redkitePool = 14
const untilTime = new Date('2021-11-09T12:00:00.000Z') // open claim
const tokenAddress = '0x36bfBb1d5B3C9b336f3D64976599B6020cA805F1' // 
const tokenAmount = '200'

const gas = 300000 
const gasPriceInGwei = '40' 

const MAX_RETRY = 50 // when request Redkite API

const redkiteAbi = JSON.parse(fs.readFileSync(`./redkite.json`, 'utf8'))

const bnbAccountPrivateKey = ''
const hackedAccountPrivateKey = ''
const beneficiary = ''

module.exports = {
  MAX_RETRY,
  redkiteAddress,
  redkitePool,
  gas,
  gasPriceInGwei,
  untilTime,
  redkiteAbi,
  tokenAddress,
  platform,
  bnbAccountPrivateKey,
  hackedAccountPrivateKey,
  beneficiary,
  tokenAmount,
}
